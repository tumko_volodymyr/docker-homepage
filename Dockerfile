FROM nginx
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY ./assets /usr/share/nginx/html/assets
COPY index.html /usr/share/nginx/html/